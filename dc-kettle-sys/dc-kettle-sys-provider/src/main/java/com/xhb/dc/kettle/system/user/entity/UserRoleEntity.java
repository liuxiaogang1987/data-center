package com.xhb.dc.kettle.system.user.entity;

import lombok.Data;

/**
 * UserRoleEntity.
 */
@Data
public class UserRoleEntity {
    private String userName;

    private String userId;

    private String roleId;

    private String userNameCn;

}
